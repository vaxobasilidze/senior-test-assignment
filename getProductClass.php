<?php

  include 'connection.php';

  //Get all products
  class GetProduct extends Conn {                            //This class just returns data from database
    protected function getProductsList(){
      $command = "select * from products";                //mySql query to select everything from products table
      $result = $this->makeConnection()->query($command); //Execute query
      if($result->num_rows > 0){                          //If result is not empty
        while($row = $result->fetch_assoc()){             //Put data in array
          $returnedData[] = $row;
        }
        return $returnedData;
      }
    }
  }

  class ViewProducts extends GetProduct {                 //This class returns retrieved data
    public function showProducts(){
      $products = $this->getProductsList();               //Calling parent class method to retrieve data
      return $products;
    }
  }

?>
