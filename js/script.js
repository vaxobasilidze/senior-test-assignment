function goToAddPage(){
  $(".productslist").animate({left: '-120%'}, 400);
  $(".addproduct").stop(true, true).delay(200).animate({left: '0%'}, 400);
  $(".actiondiv").html("<button type=\"button\" name=\"back\" class=\"back btn\" onclick=\"goToMainPage()\">Back to list</button>");
  $("h2").html("Please enter product information");
}

function goToMainPage(){
  $(".addproduct").animate({left: '120%'}, 400);
  $(".productslist").stop(true, true).delay(200).animate({left: '0%'}, 400);
  $(".actiondiv").html("<button type=\"button\" name=\"add\" class=\"addbtn btn\" style=\"margin-right: 4px;\" onclick=\"goToAddPage()\">Add new product</button><button type=\"button\" name=\"remove\" class=\"btn\" onclick='removeProducts()'>Remove selected</button>");
  $("h2").html("Product List");
}

function typeSelect(){
  var type = $(".typeselect").val();
  $(".dynamic.active").removeClass('active');
  switch (type) {
    case "disc":
      $(".disc-field").show();
      $(".disc-size").addClass('active');
      $(".book-field").hide();
      $(".furniture-field").hide();
      break;
    case "book":
      $(".disc-field").hide();
      $(".book-field").show();
      $(".book-weight").addClass('active');
      $(".furniture-field").hide();
      break;
    case "furniture":
      $(".disc-field").hide();
      $(".book-field").hide();
      $(".furniture-field").show();
      $(".dimensions-field").addClass('active');
      break;
  }
}

function removeProducts(){
  var selectedProducts = [];
  if($("input:checked").length == 0){
    alert("Please select products to delete.");
  }
  else{
    $("input:checked").each(function(){
      var id = $(this).closest(".product").attr("productid");
      selectedProducts.push(id);
    });
    $.ajax({
      type: "POST",
      url: 'removeproduct.php',
      data: {ids: selectedProducts},
      success: function (response, status) {
        if(response == "1"){
          for(var i=0; i<selectedProducts.length; i++){
            $("[productid="+selectedProducts[i]+"]").fadeOut('slow', function(){ $("[productid="+selectedProducts[i]+"]").remove(); });
          }
        }
      }
    });
  }
}
