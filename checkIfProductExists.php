<?php

  include 'connection.php';

  //Check if product with same SKU exists
  class CheckProduct extends Conn {
    protected function productExists($sku){
      $command = "select * from products where sku = '" . $sku . "'"; //mySql query to select product with entered sku from products table
      $result = $this->makeConnection()->query($command); //Execute query
      if($result->num_rows > 0){                          //If result is not empty
        return true;
      }
      else {
        return false;
      }
    }
  }

  class GetProductBySku extends CheckProduct {                   //This class returns retrieved data
    public function getProduct($sku){
      $productExists = $this->productExists($sku);               //Calling parent class method to retrieve data
      return $productExists;
    }
  }

?>
