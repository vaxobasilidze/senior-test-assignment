<?php

  include 'connection.php';

  //Product class
  class Product extends Conn {
    private $sku;
    private $name;
    private $price;
    private $type;
    private $swd;

    public function setSku($Sku){
      $this->sku = $Sku;
    }
    public function getSku(){
      return $this->sku;
    }

    public function setName($Name){
      $this->name = $Name;
    }
    public function getName(){
      return $this->name;
    }

    public function setPrice($Price){
      $this->price = $Price;
    }
    public function getPrice(){
      return $this->price;
    }

    public function setType($Type){
      $this->type = $Type;
    }
    public function getType(){
      return $this->type;
    }

    public function setSwd($Swd){
      $this->swd = $Swd;
    }
    public function getSwd(){
      return $this->swd;
    }

    //Constructor
    // function __construct($sku, $name, $price, $type, $swd){
    //   $this->sku = $sku;
    //   $this->name = $name;
    //   $this->price = $price;
    //   $this->type = $type;
    //   $this->swd = $swd;
    // }
    // Add function that uses prepared statement while inserting to prevent SQL Injection

    public function addProduct(){
      $connect = $this->makeConnection();
      $result = $connect->prepare("INSERT INTO products(type, sku, name, price, swd)VALUES(?,?,?,?,?)");
      $result->bind_param("sssds",$type,$sku,$name,$price,$swd);
      $type = $this->getType();
      $sku = $this->getSku();
      $name = $this->getName();
      $price = $this->getPrice();
      $swd = $this->getSwd();
      $result->execute();
      if($result){
        echo 1;
      }
      else {
        echo 0;
      }
    }

  }

?>
