<?php
  include 'checkIfProductExists.php';

  // Validating form inputs
  $fieldEmpty = false;
  $wrongFormatSku = false;
  $wrongFormatPrice = false;
  $wrongFormatHeight = false;
  $wrongFormatWidth = false;
  $wrongFormatLength = false;
  $wrongFormatWeight = false;
  $wrongFormatSize = false;
  $skuExists = false;
  $allInputIsOkey = true;

  $sku = $_POST['productsku'];
  $name = $_POST['productname'];
  $price = $_POST['productprice'];
  $swd = "";

  if(!preg_match("/^[a-zA-Z0-9]{1,10}$/", $sku)){ //Checking SKU format
    $wrongFormatSku = true;
    $allInputIsOkey = false;
    echo "<span class='wrong-input'>SKU must contain only letters and digits. 1 to 10 characters maximum.<br><br><span>";
  }

  if(!preg_match("/^[1-9]\d*(?:\.\d+)?(?:[kmbt])?$/", $price)){ //Checking SKU format
    $wrongFormatPrice = true;
    $allInputIsOkey = false;
    echo "<span class='wrong-input'>Price must be a number. (It must be more than 0. Do not enter the '$' sign)<br><br><span>";
  }

  $type = $_POST['typeselect'];
  if($type == "furniture"){
    $height = $_POST['height'];
    $width = $_POST['width'];
    $length = $_POST['length'];
    $swd = $height . 'x' . $width . 'x' . $length;

    if(empty($sku) || empty($name) || empty($price) || empty($height) || empty($width) || empty($length)){ //Check if fields are empty
      $fieldEmpty = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>Please fill in all fields.<br><br><span>";
    }

    //Checking if height, width and length are numbers
    if(!preg_match("/^[1-9]\d*(?:\.\d+)?(?:[kmbt])?$/", $height)){
      $wrongFormatHeight = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>Height must be a number.<br><br><span>";
    }
    if(!preg_match("/^[1-9]\d*(?:\.\d+)?(?:[kmbt])?$/", $width)){
      $wrongFormatWidth = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>Width must be a number.<br><br><span>";
    }
    if(!preg_match("/^[1-9]\d*(?:\.\d+)?(?:[kmbt])?$/", $length)){
      $wrongFormatLength = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>length must be a number.<br><br><span>";
    }
  }
  else if($type == "book"){
    $weight = $_POST['weight'];
    $swd = $weight;

    if(empty($sku) || empty($name) || empty($price) || empty($weight)){
      $fieldEmpty = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>Please fill in all fields.<br><br><span>";
    }

    if(!preg_match("/^[1-9]\d*(?:\.\d+)?(?:[kmbt])?$/", $weight)){
      $wrongFormatWeight = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>Weight must be a number.<br><br><span>";
    }
  }
  else if($type == "disc") {
    $size = $_POST['size'];
    $swd = $size;

    if(empty($sku) || empty($name) || empty($price) || empty($size)){
      $fieldEmpty = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>Please fill in all fields.<br><br><span>";
    }

    if(!preg_match("/^[1-9]\d*(?:\.\d+)?(?:[kmbt])?$/", $size)){
      $wrongFormatSize = true;
      $allInputIsOkey = false;
      echo "<span class='wrong-input'>Disc size must be a number.<br><br><span>";
    }
  }

  //Check if product with same SKU exists
  $newProduct = new GetProductBySku();
  $skuExists = $newProduct->getProduct($sku);
  if($skuExists){
    $allInputIsOkey = false;
    echo "<span class='wrong-input'>Product with same SKU already exists.<br><br><span>";
  }
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
  var fieldEmpty = "<?php echo $fieldEmpty; ?>"
  var wrongFormatSku = "<?php echo $wrongFormatSku; ?>"
  var wrongFormatPrice = "<?php echo $wrongFormatPrice; ?>"
  var wrongFormatHeight = "<?php echo $wrongFormatHeight; ?>"
  var wrongFormatWidth = "<?php echo $wrongFormatWidth; ?>"
  var wrongFormatLength = "<?php echo $wrongFormatLength; ?>"
  var wrongFormatWeight = "<?php echo $wrongFormatWeight; ?>"
  var wrongFormatSize = "<?php echo $wrongFormatSize; ?>"
  var allInputIsOkey = "<?php echo $allInputIsOkey; ?>"
  var skuExists = "<?php echo $skuExists; ?>"

  $(".input-error").removeClass('input-error');  //Clear old errors

  if(fieldEmpty == true){
    $(".active").each(function(){
      if($(this).val() == ""){
        $(this).addClass('input-error');
      }
    });
  }
  if(wrongFormatSku == true || skuExists == true){
    $(".productsku").addClass('input-error');
  }
  if(wrongFormatPrice == true){
    $(".productprice").addClass('input-error');
  }
  if(wrongFormatHeight == true){
    $(".furniture-height").addClass('input-error');
  }
  if(wrongFormatWidth == true){
    $(".furniture-width").addClass('input-error');
  }
  if(wrongFormatLength == true){
    $(".furniture-length").addClass('input-error');
  }
  if(wrongFormatWeight == true){
    $(".book-weight").addClass('input-error');
  }
  if(wrongFormatSize == true){
    $(".disc-size").addClass('input-error');
  }

  if(allInputIsOkey == true){
    var sku = "<?php echo $sku; ?>"
    var name = "<?php echo $name; ?>"
    var price = "<?php echo $price; ?>"
    var type = "<?php echo $type; ?>"
    var swd = "<?php echo $swd; ?>"
    $.ajax({
      type: "POST",
      url: 'addproduct.php',
      data: {sku: sku, name: name, price: price, type: type, swd: swd},
      success: function (obj, textstatus) {
        $(".message").html("<span class='correct-input'>Product has been added.<br><span>");
        $(".active").val("");
        $.ajax({
          type: "POST",
          url: 'getProductsList.php',
          data: { },
          success: function (response, status) {
            $(".productslist").html(response);
            //Here I could call goToMainPage() function to go to main page. The list is already updated and ready.
          }
        });
      }
    });
  }

</script>
