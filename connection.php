<?php

  class Conn {
    private $server; //Server Name
    private $uname; //User Name
    private $pass; //Password
    private $dbname; //Database Name

    protected function makeConnection(){  //This class creates connection to database
      $this->server = "localhost";
      $this->uname = "root";
      $this->pass = "";
      $this->dbname = "product";
      $connection = new mysqli($this->server, $this->uname, $this->pass, $this->dbname); //Connect to database in OOP style
      return $connection;
    }
  }

?>
