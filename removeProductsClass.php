<?php

  include 'connection.php';

  class RemoveProducts extends Conn {
    private $removingProductId;

    public function setRemovingProductId($RemovingProductId){
      $this->removingProductId = $RemovingProductId;
    }

    public function getRemovingProductId(){
      return $this->removingProductId;
    }
    // Remove function
    public function removeProduct(){
      $connect = $this->makeConnection();
      $remove = $connect->prepare("DELETE FROM products WHERE id = ?");
      $remove->bind_param("i",$id);
      $id = $this->getRemovingProductId();
      $result = $remove->execute();
      return $result;
    }
  }

?>
